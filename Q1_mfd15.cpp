#include <iostream>
#include <algorithm>
#include <climits>
#define SIZE 50
#define read(x) cin >> x
using namespace std;
struct selected {
    int max, secondMax, min, secondMin;
};
void printst(struct selected tmp) {
    cout << tmp.max << " " << tmp.secondMax << " "
         << tmp.min << " " << tmp.secondMin << "\n";
}
   


struct selected solve(int arr[], int start, int end) {
    
    struct selected curr;
    curr.max = arr[start];
    curr.min == arr[start];
    curr.secondMax = arr[start];
    curr.secondMin = arr[start];
    //base cases
    if(start == end) {
        return curr;
    }
    
    else if (end - start == 1) {
        //cout << "Array size = 2, start  = " << start << " end = " <<  end << '\n';
        curr.max = max(arr[start], arr[end]);
        curr.secondMax = min(arr[start], arr[end]);
        curr.min = curr.secondMax;
        curr.secondMin = curr.max;
        //printst(curr);
        return curr;
    }
    
    int mid = (start + end) / 2;
    struct selected left = solve(arr, start, mid);
    struct selected right = solve(arr, mid + 1, end);
    
    //if right subarray's max > left subarray's max
    if (right.max >= left.max) {
        curr.max = right.max;
        //if right's secondmax > left's max
        if(right.secondMax >= left.max)
            curr.secondMax = right.secondMax;
        else
            curr.secondMax = left.max;
    }
    //the other cases
    else {
        curr.max = left.max;
        if(left.secondMax >= right.max) 
            curr.secondMax = left.secondMax;
        else 
            curr.secondMax = right.max;
    }
    
    if (right.min <= left.min) {
        curr.min = right.min;
        if(right.secondMin <= left.min)
            curr.secondMin = right.secondMin;
        else
            curr.secondMin = left.min;
    }
    
    else {
        curr.min = left.min;
        if(left.secondMin <= right.min) 
            curr.secondMin = left.secondMin;
        else 
            curr.secondMin = right.min;
    }
  
    
   return curr;
}

int main(void) {
    int numElts, arr[SIZE], testCases;
    read(testCases); 
    while(testCases--) {
        read(numElts); 
        for(int i = 0; i < numElts; i++) {
            read(arr[i]);
        }
        struct selected ans = solved(arr, 0, numElts-1);
        cout << "Input array: ";
        for(int i = 0; i < numElts; i++) {
            cout << arr[i] << " ";
        }
        
       cout << "\nMax: " << ans.max << " "
            << "Second max: " << ans.secondMax << " "
            << "Min: " << ans.min << " "
            << "Second Min: " << ans.secondMin << "\n";
    }
    return 0;
}