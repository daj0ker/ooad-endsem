#include <bits/stdc++.h>
#include "Graph.cpp"
#include "Bfs.cpp"
#include "Dfs.cpp"
using namespace std;

int main(void) {
    Graph g {3, false};
    g.addEdge(1, 2);
    g.addEdge(2, 3);
    g.addEdge(3, 1);
    g.removeVert(2);
    g.disp();
}