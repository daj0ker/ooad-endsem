#include <iostream>

#include "module1.h"
#include "module2.h"
#include "module3.h"
#include "module4.h"
#include "Display.h"

using namespace std;

bool isConnected(Graph g, set<int> removedVertices) {
    int pos = 1;
    for(int i = 1; i <= g.numVert; i++) {
        if(removedVertices.find(i) == removedVertices.end()) {
            pos = i;
            break;
        }
    }
    Bfs bfsObj {g, pos};
    bool connected = true;
    for(unsigned i = 1; i <= g.numVert; i++) {
	    if(removedVertices.find(i) == removedVertices.end())
	    //Check if there is no edge to/from a vertex in bfs tree
		if(bfsObj.bfsTree[0][i] ==  0 && bfsObj.bfsTree[i][0] == 0) {
		    
			connected = false;
		}
	}
	return connected;
}

string gen_string(int num, int numBits, int base) {
    
    string ans = "";
    while(num) {
        int rem = num % base;
        ans.push_back('0' + rem);
        num = num / base;
    }
    
    reverse(ans.begin(), ans.end());
    
    int padZeros = numBits - ans.size();
    
    while(padZeros--) {
        ans.insert(0, "0");
    }
    
    return ans;
}

void solve() {
    
    Graph g {10, false};
    g.addEdge(1, 2);
    g.addEdge(2, 3);
    g.addEdge(3, 4);
    g.addEdge(4, 5);
    g.addEdge(5, 1);
    g.addEdge(1, 6);
    g.addEdge(2, 7);
    g.addEdge(3, 8);
    g.addEdge(4, 9);
    g.addEdge(5, 10);
    g.addEdge(7, 10);
    g.addEdge(6, 8);
    g.addEdge(6, 9);
    g.addEdge(7, 9);
    g.addEdge(8, 10);
    //g.removeVert(3);
    //g.removeVert(5);
    //set<int> removed;
    //removed.insert(3);
    //removed.insert(5);
    //cout << "isconnected " << isConnected(g, removed) << endl;
    //g.disp();
    //cout << isConnected(g, removedVert) << endl;
    
    for(int i = 1; i <= pow(2, g.numVert); i++) {
        set <int> removed;
        string curr_str = gen_string(i, g.numVert, 2);
        //cout << "Current string: " << curr_str;
        Graph tmp = g;
        //cout << "\nRemoved vertices: ";
        for(int j = 0; j < curr_str.size(); j++) {
            if(curr_str.at(j) == '1') {
                tmp.removeVert(j+1);
                removed.insert(j+1);
                //cout << (j+1) << " ";
            }
        }
        //cout << "\n";
        //cout << "isconnected: " << isConnected(tmp, removed) << '\n';
        if(!isConnected(tmp, removed)) {
            cout << "|S| = " << removed.size() << " Removed vertices: "; 
            for(auto it = removed.begin(); it != removed.end(); it++) 
                cout << *it << " ";
            cout << '\n';
        }
        
        removed.clear();
    }
    
}


int main(void) {
    solve();
}