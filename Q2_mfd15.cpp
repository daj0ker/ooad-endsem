#include <bits/stdc++.h>
#define SIZE 30
#define MID 15
using namespace std;
int ind;

int getSum(int arr[]) {
    int sum = 0;
    for(int i = 0; i < SIZE; i++) {
        sum += arr[i];
    }
    return sum;
}

void gen_random_inputs(int a[]) {
	std::random_device rd;
    std::mt19937 gen(rd()); //Mersenne twister PRNG. (Funda: 2^19937 - 1 is a mersenne prime)see the wiki page
    std::uniform_int_distribution<> dis(1, 5); 
	for(int i = 0; i < SIZE; i++) {
		a[i] = dis(gen);
	}
}

void disp(int arr[]) {
    for(int i = 0; i < SIZE; i++) {
        cout << arr[i] << " ";
    }
    cout << "\n";
}

bool greedy1(int a[]) {
    auto arr = a;
    sort(arr, arr + SIZE);
    int avg = (getSum(arr)/2) + 1;
    if(avg & 1) {
        return false;
    }
    int currSum = 0;
    for(int i = 0; i < SIZE; i++) {
        currSum += arr[i];
        if(currSum == avg) {
            ind = i;
            cout << "Subsets are: {";
            for(int j = 0; j <= i; j++) {
                cout << arr[j] << ", ";
            }
            cout << "} and {";
            for(int j = i + 1; j < SIZE; j++) {
                cout << arr[j] << ", ";
            }
            cout << "}\n";
            return true;
        }
    }
    return false;
}

bool greedy2(int a[]) {
    auto arr = a;
    sort(arr, arr+MID);
    sort(arr+MID+1, arr+SIZE, greater<int>());
    int avg = (getSum(arr)/2);
    if(avg & 1) { 
        //cout << "NO. \n";
        return false;
    }
    //disp(arr);
    int sum1 = 0, sum2 = 0;
    int i;
    cout << "The subsets are {";
    for(i = 0; i < SIZE; i++) {
        if(avg - arr[i] >= 0) {
            avg -= arr[i];
            sum1 += arr[i];
            cout << arr[i] << ", ";
            
        }
        else 
            break;
        if(avg - arr[MID + i] >= 0) {
            avg -= arr[MID + i];
            sum1 += arr[MID + i];
            cout << arr[i] << ", ";
        }
        else 
            break;
    }
    
    cout << "} Sum = " << sum1 << " {";
    
    for(int j = i; j < MID; j++) {
        cout << arr[j] << ", ";
        sum2 += arr[j];
        if(MID + i < SIZE) 
            cout << arr[MID + i] << ", ";
        else 
            break;
        sum2 += arr[MID + i];
    }
    cout << "} Sum =  " << sum2 << endl;
    
    return sum1 == sum2;
}

void run() {
    int arr[SIZE + 5];
    for(int i = 1; i <= 100; i++) {
        gen_random_inputs(arr);
        cout << "---------------TEST #" << i << " ----------------------\n";
        cout << "Input array: ";
        disp(arr);
        cout << "Array sum: " << getSum(arr) << "\n";
        cout << "Greedy1: \n";
        greedy1(arr)?cout << "TEST PASSED\n": cout << "TEST FAILED\n";
        cout << "Greedy2: \n";
        greedy2(arr)?cout << "TEST PASSED\n": cout << "TEST FAILED\n";
        cout << "------------------------------------------------------\n";
    }
    
    for(int i = 0; i<SIZE; i++) 
            arr[i] = 1;
    cout << "---------------FINAL TEST----------------------\n";
    cout << "Input array: ";
    disp(arr);
    cout << "Array sum: " << getSum(arr) << "\n";
    cout << "Greedy1: \n";
    greedy1(arr)?cout << "TEST PASSED\n": cout << "TEST FAILED\n";
    cout << "Greedy2: \n";
    greedy2(arr)?cout << "TEST PASSED\n": cout << "TEST FAILED\n";
    cout << "------------------------------------------------------\n";
}

int main(void) {
    run();
    return 0;
}